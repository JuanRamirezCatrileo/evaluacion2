/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Controller.DAO.AlumnosJpaController;
import Controller.DAO.exceptions.NonexistentEntityException;
import Controller.entity.Alumnos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Juan Ramirez
 */
@WebServlet(name = "actualizarcontroler", urlPatterns = {"/actualizarcontroler"})
public class actualizarcontroler extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet actualizarcontroler</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet actualizarcontroler at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nalumno1 = request.getParameter("nalumno");
        String nota11 = request.getParameter("nota1");
        String nota21 = request.getParameter("nota2");
        String nota31 = request.getParameter("nota3");
        String nota41 = request.getParameter("nota4");
        String accion = request.getParameter("accion");

        Alumnos alumnos = new Alumnos();

        alumnos.setNalumno1(nalumno1);
        alumnos.setNota11(nota11);
        alumnos.setNota21(nota21);
        alumnos.setNota31(nota31);
        alumnos.setNota41(nota41);

        AlumnosJpaController dao = new AlumnosJpaController();

        if (accion.equals("modificar")) {

            try {
                dao.edit(alumnos);
            } catch (Exception ex) {
                Logger.getLogger(actualizarcontroler.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("alumnos", alumnos);
            request.getRequestDispatcher("infoalumno.jsp").forward(request, response);
            }
            if (accion.equals("eliminar")) {

                try {
                    dao.destroy(nalumno1);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(actualizarcontroler.class.getName()).log(Level.SEVERE, null, ex);
                 }   
                    
                 request.getRequestDispatcher("index.jsp").forward(request, response);
                
            }
        }
    }

