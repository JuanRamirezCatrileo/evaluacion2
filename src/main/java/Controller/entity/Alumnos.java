/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Juan Ramirez
 */
@Entity
@Table(name = "alumnos")
@NamedQueries({
    @NamedQuery(name = "Alumnos.findAll", query = "SELECT a FROM Alumnos a"),
    @NamedQuery(name = "Alumnos.findByNalumno1", query = "SELECT a FROM Alumnos a WHERE a.nalumno1 = :nalumno1"),
    @NamedQuery(name = "Alumnos.findByNota11", query = "SELECT a FROM Alumnos a WHERE a.nota11 = :nota11"),
    @NamedQuery(name = "Alumnos.findByNota21", query = "SELECT a FROM Alumnos a WHERE a.nota21 = :nota21"),
    @NamedQuery(name = "Alumnos.findByNota31", query = "SELECT a FROM Alumnos a WHERE a.nota31 = :nota31"),
    @NamedQuery(name = "Alumnos.findByNota41", query = "SELECT a FROM Alumnos a WHERE a.nota41 = :nota41")})
public class Alumnos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nalumno1")
    private String nalumno1;
    @Size(max = 2147483647)
    @Column(name = "nota11")
    private String nota11;
    @Size(max = 2147483647)
    @Column(name = "nota21")
    private String nota21;
    @Size(max = 2147483647)
    @Column(name = "nota31")
    private String nota31;
    @Size(max = 2147483647)
    @Column(name = "nota41")
    private String nota41;

    public Alumnos() {
    }

    public Alumnos(String nalumno1) {
        this.nalumno1 = nalumno1;
    }

    public String getNalumno1() {
        return nalumno1;
    }

    public void setNalumno1(String nalumno1) {
        this.nalumno1 = nalumno1;
    }

    public String getNota11() {
        return nota11;
    }

    public void setNota11(String nota11) {
        this.nota11 = nota11;
    }

    public String getNota21() {
        return nota21;
    }

    public void setNota21(String nota21) {
        this.nota21 = nota21;
    }

    public String getNota31() {
        return nota31;
    }

    public void setNota31(String nota31) {
        this.nota31 = nota31;
    }

    public String getNota41() {
        return nota41;
    }

    public void setNota41(String nota41) {
        this.nota41 = nota41;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nalumno1 != null ? nalumno1.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alumnos)) {
            return false;
        }
        Alumnos other = (Alumnos) object;
        if ((this.nalumno1 == null && other.nalumno1 != null) || (this.nalumno1 != null && !this.nalumno1.equals(other.nalumno1))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.entity.Alumnos[ nalumno1=" + nalumno1 + " ]";
    }
    
}
